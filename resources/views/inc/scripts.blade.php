<!--jQuery, Bootstrap and other vendor JS-->

    <!--jQuery-->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
        $('.uploadbtn').on('click', function() {
             $('#uploader').trigger('click');
        });
 
    </script>
    <!--Bootstrap JS-->

    <script src="{{ asset('js/jquery-toast.min.js')}}"></script>
    <!--jScroll-->
    <script src="{{ asset('js/jquery.jscroll.min.js') }}"></script>

    <!--Magnific Popup-->
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>

    <!--Bootstrap Select-->
    <script src="{{ asset('vendors/bootstrap-select/js/bootstrap-select.min.js') }}"></script>

    <!--Theme JS-->
    <script src="{{ asset('js/theme.js') }}"></script>
