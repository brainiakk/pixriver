  <!-- Favicon -->
  <link rel="icon" href="images/favicon.png" type="image/png">

  <!--Bootstrap and Other Vendors-->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
  <link rel="stylesheet" href="{{ asset('css/jquery-toast.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/boxicons.css') }}">
  <link rel="stylesheet" href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('vendors/bootstrap-select/css/bootstrap-select.min.css') }}" media="screen">

  <!--Fonts-->
  <link href="{{ asset('css/css.css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic') }}" rel='stylesheet' type='text/css'>

  <!--Theme Styles-->
  <link rel="stylesheet" href="{{ asset('css/default/style.css') }}">
  <link rel="stylesheet" href="{{ asset('css/responsive/responsive.css') }}">
