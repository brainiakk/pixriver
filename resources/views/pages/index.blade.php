@extends('layouts.master')
@section('title', 'Home')
@section('content')
<section class="row upload_media mb-5">
    <div class="container mb-4">
        <upload-form></upload-form>
    </div>
</section> <!--Upload Form-->

<section class="row">
    <ul class="nav nav-justified ribbon">
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
    </ul>
</section> <!--Ribbon-->

<section class="row recent_uploads">
    <div class="container">
        <div class="row title_row">
            <h3>recent uploads</h3>
        </div>
        <uploaded-images></uploaded-images>
    </div>
</section> <!--Recent Upload-->


<section class="row">
    <ul class="nav nav-justified ribbon">
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
    </ul>
</section> <!--Ribbon-->
<div class="modal fade" id="pixa-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <pixa-modal></pixa-modal>
</div>

@endsection
