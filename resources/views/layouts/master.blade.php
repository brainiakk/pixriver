<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $appName }} | @yield('title')</title>
    @include('inc.styles')



    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

</head>
<body class="home" >
    <div id="app">
    @include('inc.navbar')

    @yield('content')
	<!-- Preloader -->
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
    </div>

    <!--jQuery, Bootstrap and other vendor JS-->
    @include('inc.scripts')
</body>
</html>
