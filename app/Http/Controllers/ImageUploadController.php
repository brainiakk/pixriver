<?php

namespace App\Http\Controllers;

use App\ImageUpload;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Image;

class ImageUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getImage(){

        $images = ImageUpload::orderBy('created_at', 'DESC')->get();
        return response()->json($images, 200);
    }
    public function upload(Request $request) {
      $this->validate($request, [
        'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
      ]);

      $this->storeImage($request);
    }
    public function pixaUpload(Request $request){
        $filename = basename($request->url);
        Image::make($request->url)->save(public_path('uploads/' . $filename));
        $upload = new ImageUpload();
        $upload->image = $filename;
        if($upload->save()){
            return response()->json(['status' => 'success','message' => 'Image Uploaded Successfully'], 200);

        }else{
            return response()->json(['status' => 'error', 'message' => 'Image Upload Failed'], 422);
        }
    }


    /**
     * Prepares a image for storing.
     *
     * @param mixed $request
     * @author Niklas Fandrich
     * @return bool
     */
    public function storeImage($request) {
      // Get file from request
      $file = $request->file('image');

      // Get filename with extension
      $filenameWithExt = $file->getClientOriginalName();

      // Get file path
      $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

      // Remove unwanted characters
      $filename = preg_replace("/[^A-Za-z0-9 ]/", '', $filename);
      $filename = preg_replace("/\s+/", '-', $filename);

      // Get the original image extension
      $extension = $file->getClientOriginalExtension();

      // Create unique file name
      $fileNameToStore = $filename.'_'.time().'.'.$extension;

      // Refer image to method resizeImage
      $save = $this->resizeImage($file, $fileNameToStore);

      return true;
    }

    /**
     * Resizes a image using the InterventionImage package.
     *
     * @param object $file
     * @param string $fileNameToStore
     * @author Niklas Fandrich
     * @return bool
     */
    public function resizeImage($file, $fileNameToStore) {
      // Resize image
      $resize = Image::make($file)->resize(600, null, function ($constraint) {
        $constraint->aspectRatio();
      })->encode('jpg');

      // Create hash value
      $hash = md5($resize->__toString());

      // Prepare qualified image name
      $image = $hash."jpg";

      // Put image to storage
      $save = Storage::put("public/uploads/{$fileNameToStore}", $resize->__toString());

      if($save) {
        return true;
      }
      return false;
    }
    public function messages()
{
    return [
        'image.required' => 'Please Select an Image Before Upload',
        'image.image' => 'Selected File is not an Image',
    ];
}

     private $image_ext = ['jpg', 'jpeg', 'png', 'gif'];
    public function uploadImage(Request $request){

        // $validator = Validator::make($request->all(), [
        //     'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        // ]);
        // if ($validator->fails()) {
        //     return response()->json(['status' => 'error', 'message' => 'Select an Image File'], 200);
        // }else{
            if ($request->hasfile('image')) {
                foreach($request->file('image') as $img)
                {
                    if (in_array($img->getClientOriginalExtension(), $this->image_ext)) {
                     
                    $filename = date('Y-m-d-') .rand(0,4000). '.' . $img->getClientOriginalExtension();
                    $location = public_path('uploads/') . $filename;

                        // Image::make($img)->save($location);
                        Image::make($img)->save($location);
                        $upload = new ImageUpload();
                        $upload->image = $filename;
                        if($upload->save()){
                            $data = response()->json(['status' => 'success','message' => 'Image Uploaded Successfully', 'image' => asset('/uploads/'.$filename), 'imageName' => $filename], 200);

                        }else{
                            $data= response()->json(['status' => 'error', 'message' => 'Image Upload Failed'], 422);
                        }
                    }else{
                        $data= response()->json(['status' => 'error', 'message' => 'Select a valid Image File!!'], 422);
                    }
                }
                return $data;
            }
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ImageUpload  $imageUpload
     * @return \Illuminate\Http\Response
     */
    public function show(ImageUpload $imageUpload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ImageUpload  $imageUpload
     * @return \Illuminate\Http\Response
     */
    public function edit(ImageUpload $imageUpload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ImageUpload  $imageUpload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ImageUpload $imageUpload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ImageUpload  $imageUpload
     * @return \Illuminate\Http\Response
     */
    public function destroy(ImageUpload $imageUpload)
    {
        try 
        {
            $imageUpload->delete(); // $request->id MUST be an array
            return response()->json(['status' => 'success','message' => 'Image Deleted Successfully'], 200);
        }
    
        catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
