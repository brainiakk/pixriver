<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('upload', 'ImageUploadController@uploadImage')->name('upload');
Route::post('pixa-upload', 'ImageUploadController@pixaUpload')->name('pixa-upload');
Route::get('images', 'ImageUploadController@getImage')->name('images');
Route::delete('image/delete/{imageUpload}', 'ImageUploadController@destroy')->name('delete.image');
