<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('pages.index');
})->name('home');

Route::get('/pictures', function () {
    return view('pages.index');
})->name('pictures');

Route::post('upload', 'ImageUploadController@upload')->name('upload');
Auth::routes();
